//
//  AppDelegate.h
//  WCSHealthKit
//
//  Created by Aaron C. Wright on 8/14/15.
//  Copyright © 2015 Wrights Creative Services, L.L.C. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

