//
//  main.m
//  WCSHealthKit
//
//  Created by Aaron C. Wright on 8/14/15.
//  Copyright © 2015 Wrights Creative Services, L.L.C. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
