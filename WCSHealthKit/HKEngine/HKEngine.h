//
//  HKEngine.h
//  WCSHealthKit
//
//  Created by Aaron C. Wright on 8/14/15.
//  Copyright © 2015 Wrights Creative Services, L.L.C. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <HealthKit/HealthKit.h>

#define ROOTCONTROLLER  [[[UIApplication sharedApplication] keyWindow] rootViewController]

/** HealthKit Data Type Read/Write */
typedef NS_ENUM(NSInteger, HKDataType) {
    /** HealthKit Read Data */
    HKDataTypeRead = 0,
    /** HealthKit Write Data */
    HKDataTypeWrite
};
/** HealthKit Error Type */
typedef NS_ENUM(NSInteger, HKErrorType) {
    /** HealthKit Error type Unknown */
    HKErrorTypeUnknown = -1,
    /** HealthKit Error type Authorization */
    HKErrorTypeAuthorization = 0,
    /** HealthKit Error type Sample error */
    HKErrorTypeSampleError
};


@class HKEngine;


@protocol HKEngineDelegate <NSObject>
@optional
/**
 *  didReceiveError is called when attempting to access HealthKit and a sample type is not authorized.
 */
- (void)didReceiveError:(HKErrorType)error;
@end


@interface HKEngine : NSObject

@property (nonatomic, weak) id<HKEngineDelegate> delegate;

@property (readonly) BOOL isAuthorized;

+ (HKEngine *)sharedEngine;
/**
 *  HealthKit authorization
 */
- (void)authorize:(void (^)(BOOL success))completion;
/** 
 *  Date of birth (MM-dd-yyyy format)
 */
- (NSString*)birthday;
/**
 *  Retrieve stored height, weight
 */
- (void)height:(void (^)(double height))completion;
- (void)weight:(void (^)(double weight))completion;
/**
 *  Steps since midnight
 */
- (void)steps:(void (^)(double steps))completion;
/**
 *  Calories (resting, active, dietary)
 */
- (void)energy:(void (^)(NSString * energy))completion;
/**
 *  Record steps (incremental), height, weight
 */
- (void)recordSteps:(double)increment completion:(void (^)(BOOL recorded, NSError * error))completion;
- (void)recordHeight:(double)height completion:(void (^)(BOOL recorded, NSError * error))completion;
- (void)recordWeight:(double)weight completion:(void (^)(BOOL recorded, NSError * error))completion;

@end
